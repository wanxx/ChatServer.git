# ChatServer

#### 介绍
基于netty的聊天服务器,搭配android客户端可直接测试，该服务器支持`socket`链接和`http`请求,提供http请求对在线用户操作

#### 软件架构
基于netty搭建的一台服务器，经测试，比较稳定，并且内存占用不高，[android客户端源代码](https://gitee.com/wanxx/ChatAndroidSdk.git)

该项目易于扩展，已经做了channel绑定、心跳检测。


#### 如何展性
- 你自己可以实现数据库链接消息存数据库
- 消息有效时长（多久没推送出去后，该消息失效）
- 做日志记录
- 如果用户在多久没收到推送（客户端app被杀死），可以使用邮件推送
- http协议已经继承，可以随心所欲自己写http接口
- 可以扩展一个后台管理系统

#### 安装教程

1. 直接运行打包好的jar文件(执行命令 java -jar ChatServer.jar)即可
    ```
    java -jar ChatServer.jar
    ```
2. 根据控制台提示发起http请求

#### 使用说明

1.  **运行StartServer.java运行主类** 
运行起后如图所示，根据控制台提示进行操作
![输入图片说明](https://images.gitee.com/uploads/images/2019/0530/202042_01c07afa_938591.png "企业微信截图_15592188013336.png")

2.  **客户端连接(通过控制台提示socket的ip跟端口进行连接到服务器)** 
连接成功会有提示，如下图所示
![输入图片说明](https://images.gitee.com/uploads/images/2019/0530/202436_6fe7a306_938591.png "企业微信截图_15592190551980.png")

`ce8a94a4e9573f54-Xiaomi-MI8` 就是我的小米手机的token

`fe707749e59ef2d8-samsung-SM-G6200` 是我三星手机的token

token 自定义，只要每个设备的token不会重复即可，为了测试方便，我要看看某个设备的具体信息，所以就使用了机型作为token

3.  **通过http推送给某个设备（通过设备推送）** 
大家都知道推送可以通过别名、设备、tag推送，其实原理都一样，就是设备唯一标识，我在这里就是用设备id推送，如果你的需求要是通过用户id推送，那在绑定channel时绑定用户id即可，也可以在自己的数据库表中维护一张用户id跟设备token的表，可自行扩展

http服务器在控制台输出了，现在试试推送给我的小米手机
http://10.102.17.221:9003/im?action=push&to=ce8a94a4e9573f54-Xiaomi-MI8&content=hell0
如下图所示
![输入图片说明](https://images.gitee.com/uploads/images/2019/0530/203947_2300c078_938591.png "企业微信截图_15592199724792.png")


4.  **查询链接情况**
http://10.102.17.221:9003/im
![输入图片说明](https://images.gitee.com/uploads/images/2019/0531/103200_0000ced0_938591.png "企业微信截图_15592699082030.png")