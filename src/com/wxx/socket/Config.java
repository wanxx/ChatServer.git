package com.wxx.socket;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class Config {
    public static String IP = getIp();

    private static String getIp() {
        try {
            InetAddress addr = InetAddress.getLocalHost();
            return addr.getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return "";
    }
}
