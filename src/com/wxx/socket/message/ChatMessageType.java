package com.wxx.socket.message;

public enum ChatMessageType {
    MSG_TYPE_PING,
    MSG_TYPE_BIND,
    MSG_TYPE_NORMAL
}
