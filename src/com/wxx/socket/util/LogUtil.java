package com.wxx.socket.util;

import java.text.SimpleDateFormat;

public class LogUtil {
    public static void log(String message) {
        String time = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss").format(System.currentTimeMillis());
        System.out.println(time + "  " + message);
    }
}
