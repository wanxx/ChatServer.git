package com.wxx.socket.server;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class ResponseJsonUtil {
    private static String KEY_SUCCESS = "success";
    private static String KEY_CODE = "code";
    private static String KEY_DATA = "data";
    private static String KEY_MESSAGE = "message";
    private static Gson gson = new Gson();

    public static String genSuccessData(Object data) {
        HashMap<String, Object> responseMap = new LinkedHashMap<>();
        responseMap.put(KEY_SUCCESS, true);
        responseMap.put(KEY_CODE, 200);
        responseMap.put(KEY_DATA, data);
        return gson.toJson(responseMap);
    }

    public static String genFailData(int code, String message) {
        HashMap<String, Object> responseMap = new LinkedHashMap<>();
        responseMap.put(KEY_SUCCESS, false);
        responseMap.put(KEY_CODE, code);
        responseMap.put(KEY_MESSAGE, message);
        return gson.toJson(responseMap);
    }

    public static String genFailData(String message) {
        return genFailData(0, message);
    }
}
